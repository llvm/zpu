//===- ZPUInstrInfo.cpp - ZPU Instruction Information ---------*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file contains the ZPU implementation of the TargetInstrInfo class.
//
//===----------------------------------------------------------------------===//

#include "ZPUInstrInfo.h"
#include "ZPUTargetMachine.h"
#include "llvm/ADT/STLExtras.h"
#include "llvm/CodeGen/MachineInstrBuilder.h"
#include "llvm/CodeGen/MachineRegisterInfo.h"
#include "llvm/Support/ErrorHandling.h"
#include "ZPUGenInstrInfo.inc"

using namespace llvm;

ZPUInstrInfo::ZPUInstrInfo(ZPUTargetMachine &tm)
  : TargetInstrInfoImpl(ZPUInsts, array_lengthof(ZPUInsts)),
    TM(tm), RI(*this)
{}

/// isLoadFromStackSlot - If the specified machine instruction is a direct
/// load from a stack slot, return the virtual or physical register number of
/// the destination along with the FrameIndex of the loaded stack slot.  If
/// not, return 0.  This predicate must return 0 if the instruction has
/// any side effects other than loading from the stack slot.
unsigned ZPUInstrInfo::
isLoadFromStackSlot(const MachineInstr *MI, int &FrameIndex) const 
{
  return 0;
}

/// isStoreToStackSlot - If the specified machine instruction is a direct
/// store to a stack slot, return the virtual or physical register number of
/// the source reg along with the FrameIndex of the loaded stack slot.  If
/// not, return 0.  This predicate must return 0 if the instruction has
/// any side effects other than storing to the stack slot.
unsigned ZPUInstrInfo::
isStoreToStackSlot(const MachineInstr *MI, int &FrameIndex) const 
{
  return 0;
}

/// insertNoop - If data hazard condition is found insert the target nop
/// instruction.
void ZPUInstrInfo::
insertNoop(MachineBasicBlock &MBB, MachineBasicBlock::iterator MI) const 
{
  llvm_unreachable("not implemented");
}

void ZPUInstrInfo::
copyPhysReg(MachineBasicBlock &MBB,
            MachineBasicBlock::iterator I, DebugLoc DL,
            unsigned DestReg, unsigned SrcReg,
            bool KillSrc) const 
{
	  llvm_unreachable("not implemented");
}

void ZPUInstrInfo::
storeRegToStackSlot(MachineBasicBlock &MBB, MachineBasicBlock::iterator I,
                    unsigned SrcReg, bool isKill, int FI, 
                    const TargetRegisterClass *RC,
                    const TargetRegisterInfo *TRI) const 
					{
					  llvm_unreachable("not implemented");
}

void ZPUInstrInfo::
loadRegFromStackSlot(MachineBasicBlock &MBB, MachineBasicBlock::iterator I,
                     unsigned DestReg, int FI,
                     const TargetRegisterClass *RC,
                     const TargetRegisterInfo *TRI) const 
{
  llvm_unreachable("not implemented");
}

