//===-- ZPUTargetMachine.cpp - Define TargetMachine for ZPU -------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// Implements the info about ZPU target spec.
//
//===----------------------------------------------------------------------===//

#include "ZPU.h"
#include "ZPUMCAsmInfo.h"
#include "ZPUTargetMachine.h"
#include "llvm/PassManager.h"
#include "llvm/Target/TargetRegistry.h"
using namespace llvm;

extern "C" void LLVMInitializeZPUTarget() {
  // Register the target.
  RegisterTargetMachine<ZPUTargetMachine> X(TheZPUTarget);
  RegisterAsmInfo<ZPUMCAsmInfo> Y(TheZPUTarget);
}


bool ZPUTargetMachine::
addInstSelector(PassManagerBase &PM, CodeGenOpt::Level OptLevel)
{
  PM.add(createZPUISelDag(*this));
  return false;
}


// DataLayout --> Big-endian, 32-bit pointer/ABI/alignment
// The stack is always 8 byte aligned
// On function prologue, the stack is created by decrementing
// its pointer. Once decremented, all references are done with positive
// offset from the stack/frame pointer, using StackGrowsUp enables
// an easier handling.
// Using CodeModel::Large enables different CALL behavior.
ZPUTargetMachine::
ZPUTargetMachine(const Target &T, const std::string &TT, const std::string &FS,
        bool isLittle):
  LLVMTargetMachine(T, TT),
  Subtarget(TT, FS, isLittle),
  DataLayout(std::string("e-p:32:32:32-i8:8:32-i16:16:32-n32")),
  InstrInfo(*this),
  FrameInfo(TargetFrameInfo::StackGrowsUp, 8, 0),
  TLInfo(*this)
{
  setRelocationModel(Reloc::Static);
}

bool ZPUTargetMachine::addPostRegAlloc(PassManagerBase &PM,
                                       CodeGenOpt::Level OptLevel) {
  PM.add(createZPUStackSlotPass());
  return true;  // -print-machineinstr should print after this.
}
