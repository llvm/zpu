//===- ZPURegisterInfo.h - ZPU Register Information Impl ------*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file contains the ZPU implementation of the TargetRegisterInfo class.
//
//===----------------------------------------------------------------------===//

#ifndef ZPUREGISTERINFO_H
#define ZPUREGISTERINFO_H

#include "ZPU.h"
#include "llvm/Target/TargetRegisterInfo.h"
#include "ZPUGenRegisterInfo.h.inc"

namespace llvm {
class TargetInstrInfo;
class Type;

class ZPURegisterInfo : public ZPUGenRegisterInfo {
public:
  ZPURegisterInfo(const TargetInstrInfo &tii);

  const TargetInstrInfo &TII;
  virtual ~ZPURegisterInfo();

  /// getRegisterNumbering - Given the enum value for some register, e.g.
  /// ZPU::RA, return the number that it corresponds to (e.g. 31).
  static unsigned getRegisterNumbering(unsigned RegEnum);

  /// Get PIC indirect call register
  static unsigned getPICCallReg();

//  /// Adjust the ZPU stack frame.
//  void adjustZPUStackFrame(MachineFunction &MF) const;

  /// Code Generation virtual methods...
  const unsigned *getCalleeSavedRegs(const MachineFunction* MF = 0) const;

  BitVector getReservedRegs(const MachineFunction &MF) const;

  bool hasFP(const MachineFunction &MF) const;

//  void eliminateCallFramePseudoInstr(MachineFunction &MF,
//                                     MachineBasicBlock &MBB,
//                                     MachineBasicBlock::iterator I) const;

  /// Stack Frame Processing Methods
  void eliminateFrameIndex(MachineBasicBlock::iterator II,
                           int SPAdj, RegScavenger *RS = NULL) const;

//  void processFunctionBeforeFrameFinalized(MachineFunction &MF) const;

  void emitPrologue(MachineFunction &MF) const;
  void emitEpilogue(MachineFunction &MF, MachineBasicBlock &MBB) const;
  
  /// Debug information queries.
  virtual unsigned getRARegister() const;
  unsigned getFrameRegister(const MachineFunction &MF) const;

  /// Exception handling queries.
  unsigned getEHExceptionRegister() const;
  unsigned getEHHandlerRegister() const;

  int getDwarfRegNum(unsigned RegNum, bool isEH) const;
};

} // end namespace llvm

#endif
