//===-- ZPU.h - Top-level interface for ZPU representation ----*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file contains the entry points for global functions defined in 
// the LLVM ZPU back-end.
//
//===----------------------------------------------------------------------===//

#ifndef TARGET_ZPU_H
#define TARGET_ZPU_H

#include "llvm/Target/TargetMachine.h"

namespace llvm {
  class ZPUTargetMachine;
  class FunctionPass;
  class MachineCodeEmitter;
  class formatted_raw_ostream;

  FunctionPass *createZPUISelDag(ZPUTargetMachine &TM);
  FunctionPass *createZPUStackSlotPass();
  extern Target TheZPUTarget;
} // end namespace llvm;

// Defines symbolic names for ZPU registers.  This defines a mapping from
// register name to register number.
#include "ZPUGenRegisterNames.inc"

// Defines symbolic names for the ZPU instructions.
#include "ZPUGenInstrNames.inc"

#endif
