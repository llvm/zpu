//===-- ZPUAsmPrinter.cpp - ZPU LLVM assembly writer --------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file contains a printer that converts from our internal representation
// of machine-dependent LLVM code to GAS-format ZPU assembly language.
//
//===----------------------------------------------------------------------===//

#define DEBUG_TYPE "ZPU-asm-printer"
#include "ZPU.h"
#include "ZPUInstrInfo.h"
#include "ZPUTargetMachine.h"
#include "llvm/BasicBlock.h"
#include "llvm/Instructions.h"
#include "llvm/CodeGen/AsmPrinter.h"
#include "llvm/CodeGen/MachineFunctionPass.h"
#include "llvm/CodeGen/MachineConstantPool.h"
#include "llvm/CodeGen/MachineFrameInfo.h"
#include "llvm/CodeGen/MachineInstr.h"
#include "llvm/MC/MCStreamer.h"
#include "llvm/MC/MCAsmInfo.h"
#include "llvm/MC/MCSymbol.h"
#include "llvm/Target/Mangler.h"
#include "llvm/Target/TargetData.h"
#include "llvm/Target/TargetLoweringObjectFile.h" 
#include "llvm/Target/TargetMachine.h"
#include "llvm/Target/TargetOptions.h"
#include "llvm/Target/TargetRegistry.h"
#include "llvm/ADT/SmallString.h"
#include "llvm/ADT/StringExtras.h"
#include "llvm/ADT/Twine.h"
#include "llvm/Support/raw_ostream.h"
using namespace llvm;

namespace {
  class ZPUAsmPrinter : public AsmPrinter {
  public:
    explicit ZPUAsmPrinter(TargetMachine &TM,  MCStreamer &Streamer)
      : AsmPrinter(TM, Streamer) {

    }

    virtual const char *getPassName() const {
      return "ZPU Assembly Printer";
    }

    bool PrintAsmOperand(const MachineInstr *MI, unsigned OpNo, 
                         unsigned AsmVariant, const char *ExtraCode,
                         raw_ostream &O);
    void printOperand(const MachineInstr *MI, int opNum, raw_ostream &O);
    void printUnsignedImm(const MachineInstr *MI, int opNum, raw_ostream &O);
    void printMemOperand(const MachineInstr *MI, int opNum, raw_ostream &O, 
                         const char *Modifier = 0);
    void printFCCOperand(const MachineInstr *MI, int opNum, raw_ostream &O, 
                         const char *Modifier = 0);
    void printSavedRegsBitmask(raw_ostream &O);
    void printHex32(unsigned int Value, raw_ostream &O);

    const char *getCurrentABIString() const;
    void emitFrameDirective();

    void printInstruction(const MachineInstr *MI, raw_ostream &O); // autogen'd.
    void EmitInstruction(const MachineInstr *MI) {
      SmallString<128> Str;
      raw_svector_ostream OS(Str);
      printInstruction(MI, OS);
      OutStreamer.EmitRawText(OS.str());
    }
    virtual void EmitFunctionBodyStart();
    virtual void EmitFunctionBodyEnd();
    virtual bool isBlockOnlyReachableByFallthrough(const MachineBasicBlock *MBB) const;
    static const char *getRegisterName(unsigned RegNo);

    void printSrcMemOperand(const MachineInstr *MI, unsigned OpNo,
                                               raw_ostream &O);
    virtual void EmitFunctionEntryLabel();
    void EmitStartOfAsmFile(Module &M);
  };
} // end of anonymous namespace

#include "ZPUGenAsmWriter.inc"

//===----------------------------------------------------------------------===//
//
//  ZPU Asm Directives
//
//  -- Frame directive "frame Stackpointer, Stacksize, RARegister"
//  Describe the stack frame.
//
//  -- Mask directives "(f)mask  bitmask, offset" 
//  Tells the assembler which registers are saved and where.
//  bitmask - contain a little endian bitset indicating which registers are 
//            saved on function prologue (e.g. with a 0x80000000 mask, the 
//            assembler knows the register 31 (RA) is saved at prologue.
//  offset  - the position before stack pointer subtraction indicating where 
//            the first saved register on prologue is located. (e.g. with a
//
//  Consider the following function prologue:
//
//    .frame  $fp,48,$ra
//    .mask   0xc0000000,-8
//       addiu $sp, $sp, -48
//       sw $ra, 40($sp)
//       sw $fp, 36($sp)
//
//    With a 0xc0000000 mask, the assembler knows the register 31 (RA) and 
//    30 (FP) are saved at prologue. As the save order on prologue is from 
//    left to right, RA is saved first. A -8 offset means that after the 
//    stack pointer subtration, the first register in the mask (RA) will be
//    saved at address 48-8=40.
//
//===----------------------------------------------------------------------===//

//===----------------------------------------------------------------------===//
// Mask directives
//===----------------------------------------------------------------------===//

// Create a bitmask with all callee saved registers for CPU or Floating Point 
// registers. For CPU registers consider RA, GP and FP for saving if necessary.
void ZPUAsmPrinter::printSavedRegsBitmask(raw_ostream &O) {
	O << "asm 3";
  }

// Print a 32 bit hex number with all numbers.
void ZPUAsmPrinter::printHex32(unsigned Value, raw_ostream &O) {
	O << "asm 4";
}

//===----------------------------------------------------------------------===//
// Frame and Set directives
//===----------------------------------------------------------------------===//

/// Frame Directive
void ZPUAsmPrinter::emitFrameDirective() {
}

/// Emit Set directives.
const char *ZPUAsmPrinter::getCurrentABIString() const { 
  return NULL;
}  

void ZPUAsmPrinter::EmitFunctionEntryLabel() {
}

/// EmitFunctionBodyStart - Targets can override this to emit stuff before
/// the first basic block in the function.
void ZPUAsmPrinter::EmitFunctionBodyStart() {
}

/// EmitFunctionBodyEnd - Targets can override this to emit stuff after
/// the last basic block in the function.
void ZPUAsmPrinter::EmitFunctionBodyEnd() {
}


/// isBlockOnlyReachableByFallthough - Return true if the basic block has
/// exactly one predecessor and the control transfer mechanism between
/// the predecessor and this block is a fall-through.
bool ZPUAsmPrinter::isBlockOnlyReachableByFallthrough(const MachineBasicBlock *MBB) 
    const {
	return false;
}

// Print out an operand for an inline asm expression.
bool ZPUAsmPrinter::PrintAsmOperand(const MachineInstr *MI, unsigned OpNo, 
                                     unsigned AsmVariant,const char *ExtraCode,
                                     raw_ostream &O) {
	O << "asm 9";
  return false;
}

void ZPUAsmPrinter::printOperand(const MachineInstr *MI, int opNum,
                                  raw_ostream &O) {
  
  if( opNum >= MI->getNumOperands())
  {
    printf("WARNING! Buggy code for now, trying to print opcode %d, count is %d \n",opNum,MI->getNumOperands());
    return;
  }
  const MachineOperand &MO = MI->getOperand (opNum);
  bool CloseParen = false;
  switch (MO.getType()) {
  case MachineOperand::MO_Register:
	O << "; %" << LowercaseString(getRegisterName(MO.getReg()));
	break;

  case MachineOperand::MO_Immediate:
	O << (int)MO.getImm();
	break;
  case MachineOperand::MO_MachineBasicBlock:
	O << *MO.getMBB()->getSymbol();
	return;
  case MachineOperand::MO_GlobalAddress:
	O << *Mang->getSymbol(MO.getGlobal());
	break;
  case MachineOperand::MO_ExternalSymbol:
	O << MO.getSymbolName();
	break;
  default:
	llvm_unreachable("<unknown operand type>");
  }
  if (CloseParen) O << ")";

}

void ZPUAsmPrinter::printUnsignedImm(const MachineInstr *MI, int opNum,
                                      raw_ostream &O) {
	O << "asm 11";
}

void ZPUAsmPrinter::
printMemOperand(const MachineInstr *MI, int opNum, raw_ostream &O,
                const char *Modifier) {

      //if(opNum+1 < MI->getNumOperands())
      //{      
      //  printOperand(MI, opNum+1, O);
      //}
	  O << ",  ";
	  printOperand(MI, opNum, O);
}

void ZPUAsmPrinter::
printFCCOperand(const MachineInstr *MI, int opNum, raw_ostream &O,
                const char *Modifier) {
	O << "asm 1";
}

void ZPUAsmPrinter::EmitStartOfAsmFile(Module &M) {
}

void ZPUAsmPrinter::printSrcMemOperand(const MachineInstr *MI, unsigned OpNo,
                                           raw_ostream &O) {
    O << "asm 15";
}


// Force static initialization.
extern "C" void LLVMInitializeZPUAsmPrinter() { 
  RegisterAsmPrinter<ZPUAsmPrinter> X(TheZPUTarget);
}
