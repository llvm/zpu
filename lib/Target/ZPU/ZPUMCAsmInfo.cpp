//===-- ZPUMCAsmInfo.cpp - ZPU asm properties ---------------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file contains the declarations of the ZPUMCAsmInfo properties.
//
//===----------------------------------------------------------------------===//

#include "ZPUMCAsmInfo.h"
using namespace llvm;

ZPUMCAsmInfo::ZPUMCAsmInfo(const Target &T, StringRef TT) {
  AlignmentIsInBytes          = false;
  Data16bitsDirective         = "\t.half\t";
  Data32bitsDirective         = "\t.word\t";
  Data64bitsDirective         = 0;
  PrivateGlobalPrefix         = "$";
  CommentString               = "#";
  ZeroDirective               = "\t.space\t";
  GPRel32Directive            = "\t.gpword\t";
  HasSetDirective             = false;
}
