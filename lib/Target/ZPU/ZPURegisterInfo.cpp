//===- ZPURegisterInfo.cpp - ZPU Register Information -== -----*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file contains the ZPU implementation of the TargetRegisterInfo class.
//
//===----------------------------------------------------------------------===//

#define DEBUG_TYPE "ZPU-reg-info"

#include "ZPU.h"
#include "ZPURegisterInfo.h"
#include "llvm/Constants.h"
#include "llvm/Type.h"
#include "llvm/Function.h"
#include "llvm/CodeGen/ValueTypes.h"
#include "llvm/CodeGen/MachineInstrBuilder.h"
#include "llvm/CodeGen/MachineFunction.h"
#include "llvm/CodeGen/MachineFrameInfo.h"
#include "llvm/CodeGen/MachineLocation.h"
#include "llvm/Target/TargetFrameInfo.h"
#include "llvm/Target/TargetMachine.h"
#include "llvm/Target/TargetOptions.h"
#include "llvm/Target/TargetInstrInfo.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Support/Debug.h"
#include "llvm/Support/ErrorHandling.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/ADT/BitVector.h"
#include "llvm/ADT/STLExtras.h"

using namespace llvm;

ZPURegisterInfo::ZPURegisterInfo(const TargetInstrInfo &tii) :
		ZPUGenRegisterInfo(),
		TII(tii)
{
}

/// getRegisterNumbering - Given the enum value for some register, e.g.
/// ZPU::RA, return the number that it corresponds to (e.g. 31).
unsigned ZPURegisterInfo::
getRegisterNumbering(unsigned RegEnum) 
{
  switch (RegEnum) {
  case ZPU::SP   : return 0;
  case ZPU::PC   : return 1;
    default: llvm_unreachable("Unknown register number!");
  }    
  return 0; // Not reached
}

//===----------------------------------------------------------------------===//
// Callee Saved Registers methods 
//===----------------------------------------------------------------------===//

BitVector ZPURegisterInfo::
getReservedRegs(const MachineFunction &MF) const
{
  BitVector Reserved(getNumRegs());
  Reserved.set(ZPU::PC);
  Reserved.set(ZPU::SP);
  Reserved.set(ZPU::FP);
  Reserved.set(ZPU::RETVAL);

  return Reserved;
}


bool ZPURegisterInfo::
hasFP(const MachineFunction &MF) const {
  return false;
}


void ZPURegisterInfo::
emitPrologue(MachineFunction &MF) const 
{
#if 0
  MachineBasicBlock &MBB   = MF.front();
  MachineFrameInfo *MFI    = MF.getFrameInfo();
  ZPUFunctionInfo *ZPUFI = MF.getInfo<ZPUFunctionInfo>();
  MachineBasicBlock::iterator MBBI = MBB.begin();
  DebugLoc dl = MBBI != MBB.end() ? MBBI->getDebugLoc() : DebugLoc();
  bool isPIC = (MF.getTarget().getRelocationModel() == Reloc::PIC_);

  // Get the right frame order for ZPU.
  adjustZPUStackFrame(MF);

  // Get the number of bytes to allocate from the FrameInfo.
  unsigned StackSize = MFI->getStackSize();

  // No need to allocate space on the stack.
  if (StackSize == 0 && !MFI->adjustsStack()) return;

  int FPOffset = ZPUFI->getFPStackOffset();
  int RAOffset = ZPUFI->getRAStackOffset();

  BuildMI(MBB, MBBI, dl, TII.get(ZPU::NOREORDER));
  
  // Adjust stack : addi sp, sp, (-imm)
  BuildMI(MBB, MBBI, dl, TII.get(ZPU::ADDiu), ZPU::SP)
      .addReg(ZPU::SP).addImm(-StackSize);

  // Save the return address only if the function isnt a leaf one.
  // sw  $ra, stack_loc($sp)
  if (MFI->adjustsStack()) { 
    BuildMI(MBB, MBBI, dl, TII.get(ZPU::SW))
        .addReg(ZPU::RA).addImm(RAOffset).addReg(ZPU::SP);
  }
#endif
}

void ZPURegisterInfo::
emitEpilogue(MachineFunction &MF, MachineBasicBlock &MBB) const 
{
#if 0
  MachineBasicBlock::iterator MBBI = prior(MBB.end());
  MachineFrameInfo *MFI            = MF.getFrameInfo();
  ZPUFunctionInfo *ZPUFI         = MF.getInfo<ZPUFunctionInfo>();
  DebugLoc dl = MBBI->getDebugLoc();

  // Get the number of bytes from FrameInfo
  int NumBytes = (int) MFI->getStackSize();

  // adjust stack  : insert addi sp, sp, (imm)
  if (NumBytes) {
    BuildMI(MBB, MBBI, dl, TII.get(ZPU::ADDiu), ZPU::SP)
      .addReg(ZPU::SP).addImm(NumBytes);
  }
#endif
}


int ZPURegisterInfo::
getDwarfRegNum(unsigned RegNum, bool isEH) const {
  llvm_unreachable("What is the dwarf register number");
  return -1;
}


unsigned ZPURegisterInfo::
getFrameRegister(const MachineFunction &MF) const
{
	  return ZPU::FP;
}

void ZPURegisterInfo::
eliminateFrameIndex(MachineBasicBlock::iterator II,
                           int SPAdj, RegScavenger *RS) const
{
	  assert(SPAdj == 0 && "Unxpected");

	  unsigned i = 0;
	  MachineInstr &MI = *II;
	  MachineFunction &MF = *MI.getParent()->getParent();
	  while (!MI.getOperand(i).isFI()) {
	    ++i;
	    assert(i < MI.getNumOperands() && "Instr doesn't have FrameIndex operand!");
	  }

	  int FrameIndex = MI.getOperand(i).getIndex();

	  unsigned BasePtr = ZPU::SP;

//	  if (MI.getOperand(i).isReg() || MI.getOperand(i).getReg() == ZPU::SP)
//		  return;

	  // This must be part of a rri or ri operand memory reference.  Replace the
	  // FrameIndex with base register with BasePtr.  Add an offset to the
	  // displacement field.
	  MI.getOperand(i).ChangeToRegister(BasePtr, false);


	  // Offset is a either 12-bit unsigned or 20-bit signed integer.
	  // FIXME: handle "too long" displacements.
	  int Offset = 0;
	  if (i + 1 < MI.getNumOperands())
	  {
		  if (MI.getOperand(i+1).isImm())
		  {
			Offset = getFrameIndexOffset(MF, FrameIndex) + MI.getOperand(i+1).getImm();
		  }

		  MI.getOperand(i+1).ChangeToImmediate(Offset);
	  }

	  // Check whether displacement is too long to fit into 12 bit zext field.
	  MI.setDesc(TII.get(MI.getOpcode()));
}

unsigned ZPURegisterInfo::
getRARegister() const
{
   llvm_unreachable("ZPU does not have a return address register");
   return 0;
}

ZPURegisterInfo::~ZPURegisterInfo()
{

}

const unsigned* ZPURegisterInfo::
getCalleeSavedRegs(const MachineFunction *MF) const
{
  static const unsigned none[] = {0};

 return none;
}

#include "ZPUGenRegisterInfo.inc"

