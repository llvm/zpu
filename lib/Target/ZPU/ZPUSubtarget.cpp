//===- ZPUSubtarget.cpp - ZPU Subtarget Information -----------*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file implements the ZPU specific subclass of TargetSubtarget.
//
//===----------------------------------------------------------------------===//

#include "ZPUSubtarget.h"
#include "ZPU.h"
#include "ZPUGenSubtarget.inc"
using namespace llvm;

ZPUSubtarget::ZPUSubtarget(const std::string &TT, const std::string &FS,
                             bool little) :
  HasSwap(false),ZPUArchVersion(ZPU1)

{
}
