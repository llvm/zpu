//===-- ZPUTargetMachine.h - Define TargetMachine for ZPU -00--*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file declares the ZPU specific subclass of TargetMachine.
//
//===----------------------------------------------------------------------===//

#ifndef ZPUTARGETMACHINE_H
#define ZPUTARGETMACHINE_H

#include "ZPUSubtarget.h"
#include "ZPUInstrInfo.h"
#include "ZPUISelLowering.h"
#include "llvm/Target/TargetMachine.h"
#include "llvm/Target/TargetData.h"
#include "llvm/Target/TargetFrameInfo.h"

namespace llvm {
  class formatted_raw_ostream;
  
  class ZPUTargetMachine : public LLVMTargetMachine {
    const TargetData    DataLayout; // Calculates type size & alignment
    ZPUInstrInfo       InstrInfo;
    TargetFrameInfo     FrameInfo;
    ZPUTargetLowering  TLInfo;
    ZPUSubtarget       Subtarget;
  public:

    ZPUTargetMachine(const Target &T, const std::string &TT, const std::string &FS,
            bool isLittle=false);
    
    virtual const ZPUInstrInfo   *getInstrInfo()     const
    { return &InstrInfo; }
    virtual const TargetFrameInfo *getFrameInfo()     const 
    { return &FrameInfo; }
    virtual const TargetData      *getTargetData()    const 
    { return &DataLayout;}
    virtual const ZPUSubtarget   *getSubtargetImpl() const
    { return &Subtarget; }
    virtual const ZPURegisterInfo *getRegisterInfo()  const {
      return &InstrInfo.getRegisterInfo();
    }
    virtual bool addPostRegAlloc(PassManagerBase &PM, CodeGenOpt::Level OptLevel);
    virtual const ZPUTargetLowering *getTargetLowering() const {
          return &TLInfo;
        };
    virtual bool addInstSelector(PassManagerBase &PM,
                                    CodeGenOpt::Level OptLevel);
  };


} // End llvm namespace

#endif
