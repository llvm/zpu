//===-- ZPUISelLowering.cpp - ZPU DAG Lowering Implementation -----------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file defines the interfaces that ZPU uses to lower LLVM code into a
// selection DAG.
//
//===----------------------------------------------------------------------===//

#define DEBUG_TYPE "ZPU-lower"
#include "ZPUISelLowering.h"
#include "ZPUTargetMachine.h"
#include "llvm/DerivedTypes.h"
#include "llvm/Function.h"
#include "llvm/GlobalVariable.h"
#include "llvm/Intrinsics.h"
#include "llvm/CallingConv.h"
#include "llvm/CodeGen/CallingConvLower.h"
#include "llvm/CodeGen/MachineFrameInfo.h"
#include "llvm/CodeGen/MachineFunction.h"
#include "llvm/CodeGen/MachineInstrBuilder.h"
#include "llvm/CodeGen/MachineRegisterInfo.h"
#include "llvm/CodeGen/SelectionDAGISel.h"
#include "llvm/CodeGen/TargetLoweringObjectFileImpl.h"
#include "llvm/CodeGen/ValueTypes.h"
#include "llvm/Support/Debug.h"
#include "llvm/Support/ErrorHandling.h"
using namespace llvm;

const char *ZPUTargetLowering::getTargetNodeName(unsigned Opcode) const {
  switch (Opcode) {
    case ZPUISD::JmpLink    : return "ZPUISD::JmpLink";
    case ZPUISD::Hi         : return "ZPUISD::Hi";
    case ZPUISD::Lo         : return "ZPUISD::Lo";
    case ZPUISD::GPRel      : return "ZPUISD::GPRel";
    case ZPUISD::Ret        : return "ZPUISD::Ret";
    case ZPUISD::CMov       : return "ZPUISD::CMov";
    case ZPUISD::SelectCC   : return "ZPUISD::SelectCC";
    case ZPUISD::FPSelectCC : return "ZPUISD::FPSelectCC";
    case ZPUISD::FPBrcond   : return "ZPUISD::FPBrcond";
    case ZPUISD::FPCmp      : return "ZPUISD::FPCmp";
    case ZPUISD::FPRound    : return "ZPUISD::FPRound";
    default                  : return NULL;
  }
}

ZPUTargetLowering::
ZPUTargetLowering(ZPUTargetMachine &TM)
  : TargetLowering(TM, new TargetLoweringObjectFileELF()) {

  // ZPU does not have i1 type, so use i32 for
  // setcc operations results (slt, sgt, ...). 
  setBooleanContents(ZeroOrOneBooleanContent);

  // Set up the register classes
  addRegisterClass(MVT::i32, ZPU::CPURegsRegisterClass);

  setOperationAction(ISD::GlobalAddress, MVT::i32, Custom);

  // Load extented operations for i1 types must be promoted 
  setLoadExtAction(ISD::EXTLOAD,  MVT::i1,  Promote);
  setLoadExtAction(ISD::ZEXTLOAD, MVT::i1,  Promote);
  setLoadExtAction(ISD::SEXTLOAD, MVT::i1,  Promote);

  // ZPU doesn't have extending float->double load/store
  setLoadExtAction(ISD::EXTLOAD, MVT::f32, Expand);
  setTruncStoreAction(MVT::f64, MVT::f32, Expand);

  // Used by legalize types to correctly generate the setcc result. 
  // Without this, every float setcc comes with a AND/OR with the result, 
  // we don't want this, since the fpcmp result goes to a flag register, 
  // which is used implicitly by brcond and select operations.
  AddPromotedToType(ISD::SETCC, MVT::i1, MVT::i32);


  //setOperationAction(ISD::SETCC,            MVT::i32,   Custom);

  // Operations not directly supported by ZPU.
  //setOperationAction(ISD::BR_JT,             MVT::Other, Expand);
  setOperationAction(ISD::BR_CC,             MVT::Other, Expand);
  //setOperationAction(ISD::BRCOND,            MVT::Other, Expand);
  setOperationAction(ISD::SELECT_CC,         MVT::Other, Expand);
  setOperationAction(ISD::UINT_TO_FP,        MVT::i32,   Expand);
  setOperationAction(ISD::FP_TO_UINT,        MVT::i32,   Expand);
  setOperationAction(ISD::SIGN_EXTEND_INREG, MVT::i1,    Expand);
  setOperationAction(ISD::CTPOP,             MVT::i32,   Expand);
  setOperationAction(ISD::CTTZ,              MVT::i32,   Expand);
  setOperationAction(ISD::ROTL,              MVT::i32,   Expand);
  setOperationAction(ISD::ROTR,              MVT::i32,   Expand);
  setOperationAction(ISD::SHL_PARTS,         MVT::i32,   Expand);
  setOperationAction(ISD::SRA_PARTS,         MVT::i32,   Expand);
  setOperationAction(ISD::SRL_PARTS,         MVT::i32,   Expand);
  setOperationAction(ISD::FCOPYSIGN,         MVT::f32,   Expand);
  setOperationAction(ISD::FCOPYSIGN,         MVT::f64,   Expand);
  setOperationAction(ISD::FSIN,              MVT::f32,   Expand);
  setOperationAction(ISD::FCOS,              MVT::f32,   Expand);
  setOperationAction(ISD::FPOWI,             MVT::f32,   Expand);
  setOperationAction(ISD::FPOW,              MVT::f32,   Expand);
  setOperationAction(ISD::FLOG,              MVT::f32,   Expand);
  setOperationAction(ISD::FLOG2,             MVT::f32,   Expand);
  setOperationAction(ISD::FLOG10,            MVT::f32,   Expand);
  setOperationAction(ISD::FEXP,              MVT::f32,   Expand);

  setOperationAction(ISD::EH_LABEL,          MVT::Other, Expand);

  // Use the default for now
  setOperationAction(ISD::STACKSAVE,         MVT::Other, Expand);
  setOperationAction(ISD::STACKRESTORE,      MVT::Other, Expand);
  setOperationAction(ISD::MEMBARRIER,        MVT::Other, Expand);

  setStackPointerRegisterToSaveRestore(ZPU::SP);
  computeRegisterProperties();
}

MVT::SimpleValueType ZPUTargetLowering::getSetCCResultType(EVT VT) const {
  return MVT::i32;
}

/// getFunctionAlignment - Return the Log2 alignment of this function.
unsigned ZPUTargetLowering::getFunctionAlignment(const Function *) const {
  return 2;
}

SDValue ZPUTargetLowering::LowerOperation(SDValue Op, SelectionDAG &DAG) const
{
  switch (Op.getOpcode()) 
  {
  default: llvm_unreachable("Wasn't expecting to be able to lower this!");
	case ISD::GlobalAddress:      return LowerGlobalAddress(Op, DAG);
	case ISD::SETCC:		 	 return LowerSETCC(Op, DAG);
  }
  return SDValue();
}

SDValue ZPUTargetLowering::LowerGlobalAddress(SDValue Op,
                                                   SelectionDAG &DAG) const {
  DebugLoc DL = Op.getDebugLoc();
  const GlobalValue *GV = cast<GlobalAddressSDNode>(Op)->getGlobal();

  return Op = DAG.getTargetGlobalAddress(GV, DL, MVT::i32);
}

//===----------------------------------------------------------------------===//
//  Lower helper functions
//===----------------------------------------------------------------------===//

// AddLiveIn - This helper function adds the specified physical register to the
// MachineFunction as a live in value.  It also creates a corresponding
// virtual register for it.
static unsigned
AddLiveIn(MachineFunction &MF, unsigned PReg, TargetRegisterClass *RC) 
{
  assert(RC->contains(PReg) && "Not the correct regclass!");
  unsigned VReg = MF.getRegInfo().createVirtualRegister(RC);
  MF.getRegInfo().addLiveIn(PReg, VReg);
  return VReg;
}

MachineBasicBlock *
ZPUTargetLowering::EmitInstrWithCustomInserter(MachineInstr *MI,
                                                MachineBasicBlock *BB) const {
    return BB;
}

//===----------------------------------------------------------------------===//
//                      Calling Convention Implementation
//===----------------------------------------------------------------------===//

#include "ZPUGenCallingConv.inc"

//===----------------------------------------------------------------------===//
// TODO: Implement a generic logic using tblgen that can support this. 
// ZPU O32 ABI rules:
// ---
// i32 - Passed in A0, A1, A2, A3 and stack
// f32 - Only passed in f32 registers if no int reg has been used yet to hold 
//       an argument. Otherwise, passed in A1, A2, A3 and stack.
// f64 - Only passed in two aliased f32 registers if no int reg has been used 
//       yet to hold an argument. Otherwise, use A2, A3 and stack. If A1 is 
//       not used, it must be shadowed. If only A3 is avaiable, shadow it and
//       go to stack.
//===----------------------------------------------------------------------===//

static bool CC_ZPUO32(unsigned ValNo, EVT ValVT,
                       MVT LocVT, CCValAssign::LocInfo LocInfo,
                       ISD::ArgFlagsTy ArgFlags, CCState &State) {

  return false; // CC must always match
}

static bool CC_ZPUO32_VarArgs(unsigned ValNo, EVT ValVT,
                       MVT LocVT, CCValAssign::LocInfo LocInfo,
                       ISD::ArgFlagsTy ArgFlags, CCState &State) {

    return false;
}

//===----------------------------------------------------------------------===//
//                  Call Calling Convention Implementation
//===----------------------------------------------------------------------===//

/// LowerCall - functions arguments are copied from virtual regs to
/// (physical regs)/(stack frame), CALLSEQ_START and CALLSEQ_END are emitted.
/// TODO: isTailCall.
SDValue
ZPUTargetLowering::LowerCall(SDValue Chain, SDValue Callee,
                              CallingConv::ID CallConv, bool isVarArg,
                              bool &isTailCall,
                              const SmallVectorImpl<ISD::OutputArg> &Outs,
                              const SmallVectorImpl<SDValue> &OutVals,
                              const SmallVectorImpl<ISD::InputArg> &Ins,
                              DebugLoc dl, SelectionDAG &DAG,
                              SmallVectorImpl<SDValue> &InVals) const {
  return Chain;
}

/// LowerCallResult - Lower the result values of a call into the
/// appropriate copies out of appropriate physical registers.
SDValue
ZPUTargetLowering::LowerCallResult(SDValue Chain, SDValue InFlag,
                                    CallingConv::ID CallConv, bool isVarArg,
                                    const SmallVectorImpl<ISD::InputArg> &Ins,
                                    DebugLoc dl, SelectionDAG &DAG,
                                    SmallVectorImpl<SDValue> &InVals) const {

  // Assign locations to each value returned by this call.
  SmallVector<CCValAssign, 16> RVLocs;
  CCState CCInfo(CallConv, isVarArg, getTargetMachine(),
                 RVLocs, *DAG.getContext());

  CCInfo.AnalyzeCallResult(Ins, RetCC_ZPU);

  // Copy all of the result registers out of their specified physreg.
  for (unsigned i = 0; i != RVLocs.size(); ++i) {
    Chain = DAG.getCopyFromReg(Chain, dl, RVLocs[i].getLocReg(),
                               RVLocs[i].getValVT(), InFlag).getValue(1);
    InFlag = Chain.getValue(2);
    InVals.push_back(Chain.getValue(0));
  }

  return Chain;
}

//===----------------------------------------------------------------------===//
//             Formal Arguments Calling Convention Implementation
//===----------------------------------------------------------------------===//

/// LowerFormalArguments - transform physical registers into virtual registers 
/// and generate load operations for arguments places on the stack.
SDValue
ZPUTargetLowering::LowerFormalArguments(SDValue Chain,
                                        CallingConv::ID CallConv, bool isVarArg,
                                        const SmallVectorImpl<ISD::InputArg>
                                        &Ins,
                                        DebugLoc dl, SelectionDAG &DAG,
                                        SmallVectorImpl<SDValue> &InVals)
                                          const {

	/* FIX! no arguments yet! */
  return Chain;
}

//===----------------------------------------------------------------------===//
//               Return Value Calling Convention Implementation
//===----------------------------------------------------------------------===//

SDValue
ZPUTargetLowering::LowerReturn(SDValue Chain,
                                CallingConv::ID CallConv, bool isVarArg,
                                const SmallVectorImpl<ISD::OutputArg> &Outs,
                                const SmallVectorImpl<SDValue> &OutVals,
                                DebugLoc dl, SelectionDAG &DAG) const {
	return Chain;
}

//===----------------------------------------------------------------------===//
//                           ZPU Inline Assembly Support
//===----------------------------------------------------------------------===//

/// getConstraintType - Given a constraint letter, return the type of
/// constraint it is for this target.
ZPUTargetLowering::ConstraintType ZPUTargetLowering::
getConstraintType(const std::string &Constraint) const 
{
  return TargetLowering::getConstraintType(Constraint);
}

/// Examine constraint type and operand type and determine a weight value.
/// This object must already have been set up with the operand type
/// and the current alternative constraint selected.
TargetLowering::ConstraintWeight
ZPUTargetLowering::getSingleConstraintMatchWeight(
    AsmOperandInfo &info, const char *constraint) const {
  ConstraintWeight weight = CW_Invalid;
  return weight;
}

/// getRegClassForInlineAsmConstraint - Given a constraint letter (e.g. "r"),
/// return a list of registers that can be used to satisfy the constraint.
/// This should only be used for C_RegisterClass constraints.
std::pair<unsigned, const TargetRegisterClass*> ZPUTargetLowering::
getRegForInlineAsmConstraint(const std::string &Constraint, EVT VT) const
{
  return TargetLowering::getRegForInlineAsmConstraint(Constraint, VT);
}

/// Given a register class constraint, like 'r', if this corresponds directly
/// to an LLVM register class, return a register of 0 and the register class
/// pointer.
std::vector<unsigned> ZPUTargetLowering::
getRegClassForInlineAsmConstraint(const std::string &Constraint,
                                  EVT VT) const
{
  if (Constraint.size() != 1)
    return std::vector<unsigned>();

  return std::vector<unsigned>();
}

bool
ZPUTargetLowering::isOffsetFoldingLegal(const GlobalAddressSDNode *GA) const {
  // The ZPU target isn't yet aware of offsets.
  return true;
}

bool ZPUTargetLowering::isFPImmLegal(const APFloat &Imm, EVT VT) const {
  if (VT != MVT::f32 && VT != MVT::f64)
    return false;
  return Imm.isZero();
}
#include <stdio.h>
static SDValue EmitConditionFunc(SDValue LHS,SDValue RHS,ISD::CondCode CC,
                       DebugLoc dl, SelectionDAG &DAG)
{
   bool unsign=false;
   bool swap=false;
   bool emit_equal = false;
   printf("opcode is %d \n",CC);
   switch(CC)
   {
	case ISD::SETFALSE:     //  Always false (always folded)
    case ISD::SETOEQ:       //  True if ordered and equal
    case ISD::SETOGT:       //  True if ordered and greater than
    case ISD::SETOGE:       //  True if ordered and greater than or equal
    case ISD::SETOLT:       //  True if ordered and less than
    case ISD::SETOLE:       //  True if ordered and less than or equal
    case ISD::SETONE:       //  True if ordered and operands are unequal
    case ISD::SETO:         //  True if ordered (no nans)
    case ISD::SETUO:        //  True if unordered: isnan(X) | isnan(Y)
	case ISD::SETUEQ:       //  True if unordered or equal
	case ISD::SETUGT:       //  True if unordered or greater than
    case ISD::SETUGE:       //  True if unordered, greater than, or equal
    case ISD::SETULT:       //  True if unordered or less than
    case ISD::SETULE:       //  True if unordered, less than, or equal
    case ISD::SETUNE:       //  True if unordered or not equal
    case ISD::SETTRUE:      //  Always true (always folded)
    case ISD::SETTRUE2:     //  Always true (always folded)
	case ISD::SETFALSE2:    //  Always false (always folded)
			llvm_unreachable("Unkown cmp instruction");
	break;
    case ISD::SETEQ:        //  True if equal
		return DAG.getNode(ZPUISD::EQ, dl,MVT::i32, LHS,RHS);
	case ISD::SETNE:        //  True if not equal
		return DAG.getNode(ZPUISD::NEQ, dl,MVT::i32, LHS,RHS);
	case ISD::SETLT:        //  True if less than
		swap=false;
		emit_equal=false;
		break;
    case ISD::SETLE:        //  True if less than or equal
		swap=false;
		emit_equal=true;
		break;
	case ISD::SETCC_INVALID:
		llvm_unreachable("Invalid CC");
		break;		
	case ISD::SETGE:        //  True if greater than or equal
		swap=true;
		emit_equal=true;
		break;	
	case ISD::SETGT:       //  True if greater than
		swap=true;
		emit_equal=false;
		break;
	default:
		llvm_unreachable("unimplemented operand");
	}
	SDValue cmp_node;
	unsigned opcode=0;
	if(emit_equal)
	{
		if(unsign)
		{
			opcode = ZPUISD::ULESSTHANEQ;
		}
		else
		{
			opcode = ZPUISD::LESSTHANEQ;
		}
	}
	else
	{
		if(unsign)
		{
			opcode = ZPUISD::ULESSTHAN;
		}
		else
		{
			opcode = ZPUISD::LESSTHAN;
		}
	}
	
	
	if(swap)
	{
		return DAG.getNode(opcode, dl,MVT::i32, RHS,LHS);
	}
	else
	{
		return DAG.getNode(opcode, dl,MVT::i32, LHS,RHS);	
	}
	/*
	if(negate)
	{
		SDValue cmp_res = DAG.getCopyFromReg(DAG.getEntryNode(), dl, , MVT::i32);
    
		SDValue minus1 = DAG.getConstant(-1, MVT::i32);
   
		
		return DAG.getNode(ISD::XOR,dl,MVT::i32,cmp_node,minus1);
	}
	else
	{
		return cmp_node;
	}*/
}

SDValue ZPUTargetLowering::LowerSETCC(SDValue Op, SelectionDAG &DAG) const {
  SDValue LHS   = Op.getOperand(0);
  SDValue RHS   = Op.getOperand(1);
  DebugLoc dl   = Op.getDebugLoc();

  ISD::CondCode CC = cast<CondCodeSDNode>(Op.getOperand(2))->get();
  
  return EmitConditionFunc(LHS,RHS, CC,dl,DAG);
}
