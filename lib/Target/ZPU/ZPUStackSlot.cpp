//===-- ZPUFloatingPoint.cpp - Floating point Reg -> Stack converter ------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file defines the pass which converts floating point instructions from
// pseudo registers into register stack instructions.  This pass uses live
// variable information to indicate where the FPn registers are used and their
// lifetimes.
//
// The x87 hardware tracks liveness of the stack registers, so it is necessary
// to implement exact liveness tracking between basic blocks. The CFG edges are
// partitioned into bundles where the same FP registers must be live in
// identical stack positions. Instructions are inserted at the end of each basic
// block to rearrange the live registers to match the outgoing bundle.
//
// This approach avoids splitting critical edges at the potential cost of more
// live register shuffling instructions when critical edges are present.
//
//===----------------------------------------------------------------------===//

#define DEBUG_TYPE "ZPU-codegen"
#include "ZPU.h"
#include "ZPUInstrInfo.h"
#include "llvm/ADT/DepthFirstIterator.h"
#include "llvm/ADT/DenseMap.h"
#include "llvm/ADT/SmallPtrSet.h"
#include "llvm/ADT/SmallVector.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/ADT/STLExtras.h"
#include "llvm/CodeGen/MachineFunctionPass.h"
#include "llvm/CodeGen/MachineInstrBuilder.h"
#include "llvm/CodeGen/MachineRegisterInfo.h"
#include "llvm/CodeGen/MachineFrameInfo.h"
#include "llvm/CodeGen/MachineOperand.h"
#include "llvm/CodeGen/Passes.h"
#include "llvm/Support/Debug.h"
#include "llvm/Support/ErrorHandling.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Target/TargetInstrInfo.h"
#include "llvm/Target/TargetMachine.h"
#include <algorithm>
#include <stdio.h>

using namespace llvm;


namespace
{
  struct ZPUStackSlot:public MachineFunctionPass
  {
    static char ID;
      ZPUStackSlot ():MachineFunctionPass (ID)
    {
    }

    virtual void getAnalysisUsage (AnalysisUsage & AU) const
    {
      AU.setPreservesCFG ();
      AU.addPreservedID (MachineLoopInfoID);
      AU.addPreservedID (MachineDominatorsID);
      MachineFunctionPass::getAnalysisUsage (AU);
    }

    virtual bool runOnMachineFunction (MachineFunction & MF);
    virtual bool assignStackSlots (MachineFunction & Fn, int *RegToStack);
    virtual void insertStackInstructions (MachineFunction & Fn);

    virtual const char *getPassName () const
    {
      return "ZPU Stackslotifier";
    }

  };
  char ZPUStackSlot::ID = 0;
}

FunctionPass *
llvm::createZPUStackSlotPass ()
{
  return new ZPUStackSlot ();
}

// This function will replace registers with frameindex 
bool ZPUStackSlot::assignStackSlots (MachineFunction & Fn, int *RegToStack)
{
  bool
    foundmore = false;
  const TargetInstrInfo *
    TII = Fn.getTarget ().getInstrInfo ();
  for (MachineFunction::iterator MFI = Fn.begin (), E = Fn.end (); MFI != E;
       ++MFI)
    {
      MachineBasicBlock *
    MBB = MFI;


    for (MachineBasicBlock::iterator MBBI = MBB->begin (), EE = MBB->end ();
       MBBI != EE; ++MBBI)
    {
      const  MachineInstr &MI = *MBBI;
      DebugLoc dl = MI.getDebugLoc ();

      //MBBI->dump();                 
      bool found = false;
      printf("Testing \n");
      MI.dump();
      for (unsigned i = 0, e = MI.getNumOperands (); i != e; ++i)
      {
        const MachineOperand &MO = MI.getOperand (i);
        
        if (MO.isReg ())
        {
          unsigned Reg = MO.getReg ();
          if (Reg >= ZPU::R0 && Reg <= ZPU::R3)
            {
              /* yes, we need to patch up this instruction! */
              found = true;
              break;
            }
        }
      }

      if (found)
      {
        foundmore = true;
        MachineInstr & MI = *MBBI;
      
        MachineInstrBuilder b = BuildMI (*MBB, MBBI, dl, TII->get (MI.getOpcode ()));
        printf("Selected \n");
        MI.dump();
        for (unsigned i = 0, e = MI.getNumOperands (); i != e; i++)
        {
            const MachineOperand &MO = MI.getOperand (i);

            if (MO.isReg ())
            {
              unsigned Reg = MO.getReg ();
              if (Reg >= ZPU::R0 && Reg <= ZPU::R3)
              {
                int FrameIdx;
                MachineFrameInfo *MFI = Fn.getFrameInfo ();
                
                // Determine if a new stack slot is required
                if (RegToStack[Reg] == -1)
                {
                  FrameIdx = MFI->CreateStackObject (4, 4, true);

                  RegToStack[Reg] = FrameIdx;
                }
                else
                {
                  FrameIdx = RegToStack[Reg];
                }

                b.addFrameIndex (FrameIdx);
              }
              else   
              {
                ((MachineInstr *) b)->
                  addOperand (MI.getOperand (i));
              }
            }
            else
            {
              ((MachineInstr *) b)->addOperand (MI.getOperand (i));
            }
        }

        printf("Created \n");
        ((MachineInstr *) b)->dump();
        printf("remove \n");
        MI.dump();
        
        MI.eraseFromParent ();
        
        // Crappy way out getting the iterator right, slow for big bb.
        MBBI = MBB->begin ();
      }
    }
  }
  printf("\n");
  return foundmore;
}

// This function will insert loadsp/storesp instructions.
// For each RHS argument an loadsp will be inserted. 
// For each LHS argument an storesp will be inserted.
//
//  [sp+0] = im ...
//  [sp+1] = im ...
//  [sp+0] = add [sp+0], [sp+1]
// 
//  im 
//  storesp 0   
//  im 
//  storesp 1
//  loadsp 1 
//  loadsp 1
//  add
//  storesp 1
//
//  STACKSTORE/STACKLOAD instructions should be removed since
//
//  ZPUSTORSTACKSLOT fi#2,0,R0
//  is
//  loadsp [R0]
//  storesp fi#2 
void
ZPUStackSlot::insertStackInstructions (MachineFunction & Fn)
{
  const TargetInstrInfo *TII = Fn.getTarget ().getInstrInfo ();
  for (MachineFunction::iterator MFI = Fn.begin (), E = Fn.end (); MFI != E;
       ++MFI)
  {
      MachineBasicBlock *MBB = MFI;

      for (MachineBasicBlock::iterator MBBI = MBB->begin (), EE = MBB->end ();
       MBBI != EE; ++MBBI)
    {
      /*
       * MBBI points to a basic block machine instruction
       */
      MachineInstr & MI = *MBBI;
      DebugLoc dl = MI.getDebugLoc ();

      //MBBI->dump();         
      //MBB->dump();

      int loadSPIndex = 0;
      MI.dump();
      
      // Loop backwards and start to with loadsp
      for (int i = MI.getNumOperands () - 1; i >= 0; i--)
      {
        const MachineOperand & MO = MI.getOperand (i);

        // Operand zero is LHS and should be a storesp
        MachineInstr *tempInstr;
        if (i == 0)
        {
          MachineInstr *tempInstr = NULL;
          
          if (MO.getType () == MachineOperand::MO_FrameIndex)
          {
            tempInstr =  MBB->getParent ()-> CreateMachineInstr (TII->get (ZPU::ZPUPSEUDOSTORESP),dl);

            MachineInstrBuilder (tempInstr).addImm ( (MO.getIndex () + 1)*4);
          }
          else if(MO.getType() == MachineOperand::MO_GlobalAddress)
          {
            tempInstr = MBB->getParent ()->CreateMachineInstr (TII->get (ZPU::ZPUIM),dl);
            
            MachineInstrBuilder (tempInstr).addOperand (MO);        
          }
          if( MI.getOpcode() == ZPU::ZPUSTOREREG)
          {
            // Store operations needs to have its destination pushed on stack
            // Put before instruction
            MBB->insert(MBBI, tempInstr);
          }
          else
          {
            assert(tempInstr->getOpcode() == ZPU::ZPUPSEUDOSTORESP && "The result of the instruction should be stored to stack. With a storesp");
            // Add the storesp after the instruction 
            MBB->insertAfter (MBBI, tempInstr);
            MBBI++;
          }
         
          if(MI.getOpcode() == ZPU::ZPUSTORSTACKSLOT ||
             MI.getOpcode() == ZPU::ZPULOADSTACKSLOT)
          {
            // Remove instruction if stackstore since this is same as storesp
            MI.removeFromParent();
          }
        }
        else
        {
          MachineInstr *tempInstr = NULL;
          
          printf("Operand type is %d, %d \n",MO.getType(),i);
          if (MO.getType () == MachineOperand::MO_FrameIndex)
          {
            tempInstr = MBB->getParent ()->CreateMachineInstr (TII->get (ZPU::ZPUPSEUDOLOADSP),dl);
            MachineInstrBuilder (tempInstr).addImm ( (MO.getIndex () + loadSPIndex)*4 );
          }
          else if(MO.getType() == MachineOperand::MO_GlobalAddress)
          {
            tempInstr = MBB->getParent ()->CreateMachineInstr (TII->get (ZPU::ZPUIM),dl);
            
            MachineInstrBuilder (tempInstr).addOperand (MO);        
          }
          else
          {
             if(MI.getOpcode() == ZPU::ZPUSTORSTACKSLOT ||
                MI.getOpcode() == ZPU::ZPULOADSTACKSLOT)
             {
                // xxxxSTACKSLOT have a constant, do something with this?
             }
             else
             {
                assert(0 && "Don't kow what to do with this");
             }
          }
          if(tempInstr!=NULL)
          {
            MBB->insert (MBBI, tempInstr);
          }
          // The second loadsp should have its SP increased by one since the first loadsp
          // takes one stack slot.
          loadSPIndex++;
        }
      }
    }
  }
}

/// runOnMachineFunction - Loop over all of the basic blocks, transforming FP
/// register references into FP stack references.
///
bool ZPUStackSlot::runOnMachineFunction (MachineFunction & Fn)
{
  bool
    ret;
  // Reg to stack mapping, replace by proper map
  int
    RegToStack[1024];

  memset (&RegToStack[0], -1, sizeof (RegToStack[0]) * 1024);

  ret = assignStackSlots (Fn, RegToStack);
  insertStackInstructions (Fn);

  return ret;
}
