//=====-- ZPUMCAsmInfo.h - ZPU asm properties ---------------*- C++ -*--====//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file contains the declaration of the ZPUMCAsmInfo class.
//
//===----------------------------------------------------------------------===//

#ifndef ZPUTARGETASMINFO_H
#define ZPUTARGETASMINFO_H

#include "llvm/ADT/StringRef.h"
#include "llvm/MC/MCAsmInfo.h"

namespace llvm {
  class Target;
  
  class ZPUMCAsmInfo : public MCAsmInfo {
  public:
    explicit ZPUMCAsmInfo(const Target &T, StringRef TT);
  };

} // namespace llvm

#endif
